<?php
session_start();

require 'vendor/autoload.php';

$router = new Router();
require 'core/routes.php';

$uri = trim(isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/', '/');
$uri = explode('?',$uri)[0];

try {
    $router->direct($uri);
} catch (Exception $e) {
    header('Location: /');
}

