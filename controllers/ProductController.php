<?php


class ProductController
{
    public function index()
    {
        if (isset($_SESSION['userId'])):
            $user = UserModel::getById($_SESSION['userId']);
            if ($user->isAdmin()):
                require 'views/header.view.php';
                require 'views/importProduct.view.php';
                require 'views/footer.temp.view.php';
                exit();
            endif;
        endif;

        throw new Exception('Can\'t find the webpage!!', '404');

    }

    public function getProductImage()
    {
        $product = ProductModel::getById(isset($_GET['id']) ? $_GET['id'] : 1);
        header('content-type: ' . $product->getPicMime());
        echo $product->getPicture();
    }

    public function importProduct()
    {
        if (isset($_FILES)):
            $productNumber = $_POST['productNumber'];
            $name = $_POST['name'];
            $picture = file_get_contents($_FILES['productPicture']['tmp_name']);
            $picMime = $_FILES['productPicture']['type'];
            $type = explode('/',$picMime);

            if ($type[0] === 'image'):
                $description = $_POST['description'];
                $price = $_POST['price'];

                $product = new ProductModel();
                $product->setProductNumber($productNumber);
                $product->setName($name);
                $product->setPicture($picture);
                $product->setPicMime($picMime);
                $product->setDescription($description);
                $product->setPrice($price);

                $product->saveToDB();

                header('Location: /importproduct');
            else:
                header('Location: /importproduct?err=Invalid%20file');
            endif;
        endif;
    }

    public function importFromCSV()
    {
        $csvPath = "D:\Windesheim ADSD20\Import products.csv";

        if (file_exists($csvPath)):
            $importCSV = fopen($csvPath, 'r');

            while (!feof($importCSV)) {
                $data = fgetcsv($importCSV, 0, ';');

                try {
                    if ($data && file_exists($data[4])):
                        $productNumber = intval($data[0]);
                        $name = $data[1];
                        $picture = file_get_contents($data[4]);
                        $pictureType = mime_content_type($data[4]);
                        $description = $data[2];
                        $price = floatval([3]);

                        $product = new ProductModel();
                        $product->setProductNumber($productNumber);
                        $product->setName($name);
                        $product->setPicture($picture);
                        $product->setPicMime($pictureType);
                        $product->setDescription($description);
                        $product->setPrice($price);

                        $product->saveToDB();

                    endif;
                } catch (Exception $e) {

                }
            }
            fclose($importCSV);
            echo "Imported";
        endif;
    }

}