<?php


class ShoppingCartController
{
    public function index()
    {
        $articles = self::getShoppingCart();
        $products = self::getProductInShoppingCart();

        require 'views/header.view.php';
        require 'views/shoppingcards.view.php';
        require 'views/footer.temp.view.php';
    }

    private function getProductInShoppingCart()
    {
        $articles = self::getShoppingCart();
        $products = [];

        if ($articles !== false):
            foreach (array_keys($articles) as $productId):
                $product = ProductModel::getById($productId);
                $products[$productId] = $product;
            endforeach;
        endif;

        return $products;
    }

    public function addProductToShoppingCart()
    {
        self::addToShoppingCart($_POST['productId'], $_POST['quantity']);
        header('Location: /');

    }

    public function removeProductFromShoppingCart()
    {
        unset($_SESSION['shoppingCart'][$_POST['productId']]);
        header('Location: /winkelwagen');
    }

    public function increaseProduct()
    {
        self::addToShoppingCart($_POST['productId'], 1);
        header('Location: /winkelwagen');
    }

    public function decreaseProduct()
    {
        self::addToShoppingCart($_POST['productId'], -1);
        header('Location: /winkelwagen');
    }

    public static function addToShoppingCart($productId, $quantity): void
    {
        if (isset($_SESSION['shoppingCart']) && array_key_exists($productId, $_SESSION['shoppingCart'])):
            $earlierAdded = $_SESSION['shoppingCart'][$productId]['quantity'];
        else:
            $earlierAdded = 0;
        endif;

        $earlierAdded += $quantity;

        if ($earlierAdded > 0):
            $_SESSION['shoppingCart'][$productId]['quantity'] = $earlierAdded;
        else:
            unset($_SESSION['shoppingCart'][$productId]);
        endif;
    }

    public static function getShoppingCart()
    {
        if (isset($_SESSION['shoppingCart'])):
            return $_SESSION['shoppingCart'];
        else:
            return false;
        endif;
    }

    public function saleInfo()
    {
        if (isset($_SESSION['shoppingCart']) && count($_SESSION['shoppingCart']) > 0):
            if (isset($_POST['email']) && isset($_POST['password'])):
                $user = UserModel::getByEmail($_POST['email']);

                if ($user !== false && $user->getPassword() == sha1($_POST['password'])):
                    $_SESSION['userId'] = $user->getId();
                    $_SESSION['userName'] = $user->getName();
                    $_SESSION['userAdmin'] = $user->isAdmin();
                else:
                    $user = false;
                endif;
            elseif (isset($_SESSION['userId'])):
                $user = UserModel::getById($_SESSION['userId']);
            else:
                $user = false;
            endif;

            $articles = self::getShoppingCart();
            $products = self::getProductInShoppingCart();

            require 'views/header.view.php';
            require 'views/saleInfo.view.php';
            require 'views/footer.temp.view.php';
        else:
            header('Location: /');
        endif;
    }

    public function finishShopping()
    {
        if (isset($_SESSION['shoppingCart']) && count($_SESSION['shoppingCart']) > 0 && isset($_SESSION['userId'])):
            $articles = self::getShoppingCart();
            $products = self::getProductInShoppingCart();

            if (count($products) > 0 && $articles !== false):
                try {
                    $order = new SalesOrder();
                    $order->setOrderDate(date('d M Y'));
                    $order->setOrderNumber(SalesOrder::getLastOrderNumber() + 1);
                    $order->setUserId($_SESSION['userId']);

                    $rowsList = [];
                    foreach (array_keys($articles) as $productId):
                        $product = $products[$productId];
                        $orderRow = new SalesOrderRows();
                        $orderRow->setProductId($productId);
                        $orderRow->setQuantity($articles[$productId]['quantity']);
                        $orderRow->setPrice($product->getPrice());

                        array_push($rowsList, $orderRow);
                    endforeach;

                    $order->setRows($rowsList);
                    $order->saveToDB();

                    $mailSent = self::mailBuyer($order->getOrderNumber());
                    unset($_SESSION['shoppingCart']);

                    if ($mailSent === true):
                        header('Location: /');
                    else:
                        header('Location: /nietgevonden');
                    endif;
                } catch (Exception $e) {
                    self::saleInfo();
                }
            endif;
        else:
            self::saleInfo();
        endif;
    }

    public static function mailBuyer($orderNumber)
    {
        try {
            $user = UserModel::getById($_SESSION['userId']);

            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'wfsdad20b3@gmail.com';
            $mail->Password = 'Almere2020!';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('wfsdad20b3@gmail.com', 'ADSD20 team B3');
            $mail->addAddress($user->getEmail(), $user->getName());
            $mail->isHTML(true);
            $mail->Subject = 'Flevosap order nummer: ' . $orderNumber;
            $mail->addEmbeddedImage('asset/Pictures/logo-headerV3.png', 'Logo', 'LogoFlevosap');
            $mail->Body = "<h1>Uw bestelling met ordernummer $orderNumber</h1>
                            <br />
                            <br />
                            Wij hebben uw bestelling in goede orde ontvangen en zullen we deze zo spoedig mogelijk in behandeling nemen.
                            <br />
                            <br />
                            Met vriendelijke groet,<br >
                            Flevosap<br />
                            <img src=\"cid:Logo\" alt=\"Flevosap\">";

            $mail->send();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

}