<?php


class HomeController
{
    public function index()
    {
        $products = ProductModel::getAll();

        require 'views/header.view.php';
        require 'views/productcard.view.php';
        require 'views/footer.temp.view.php';
    }

    public function notFound()
    {
        require 'views/header.view.php';
        require 'views/error404.view.php';
        require 'views/footer.temp.view.php';
    }

}