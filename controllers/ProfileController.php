<?php


class ProfileController
{
    public function index()
    {
        $user=UserModel::getById($_SESSION['userId']);
        require 'views/header.view.php';
        require 'views/profile.view.php';
        require 'views/footer.temp.view.php';
    }

    public function index2()
    {
        $user=UserModel::getById($_SESSION['userId']);
        require 'views/header.view.php';
        require 'views/passChange.view.php';
        require 'views/footer.temp.view.php';
    }

    public function updateInfo()
    {
        $user=UserModel::getById($_SESSION['userId']);
        $user->setName($_POST['name']);
        $user->setEmail($_POST['email']);
        $user->setHomeNumber($_POST['homeNumber']);
        $user->setState($_POST['state']);
        $user->setStreet($_POST['street']);
        $user->setZipcode($_POST['zipcode']);

        $user->saveToDB();
        header('Location: /');
    }

    public function updatePassword()
    {
        $user=UserModel::getById($_SESSION['userId']);
        $user->setPassword($_POST['password']);
        $user->saveToDB();
        header('Location: /');
    }
}