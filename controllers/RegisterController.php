<?php


class RegisterController
{
    public function index()
    {
        require 'views/header.view.php';
        require 'views/register.view.php';
        require 'views/footer.temp.view.php';
    }

    public function register()
    {

        if (isset($_POST['email']) && strlen($_POST['email']) > 0 && isset($_POST['password']) && strlen($_POST['password']) > 0) :
            $user = UserModel::getByEmail($_POST['email']);

            if ($user === false) :
                $user = new UserModel();
                $user->setName($_POST['name']);
                $user->setPassword($_POST['password']);
                $user->setEmail($_POST['email']);
                $user->setStreet($_POST['street']);
                $user->setHomeNumber($_POST['homeNumber']);
                $user->setZipcode($_POST['zipcode']);
                $user->setState($_POST['state']);
                $user->setAdmin(false);
                $user->saveToDB();

                $_SESSION['userId'] = $user->getId();
                $_SESSION['userName'] = $user->getName();
                $_SESSION['userAdmin'] = $user->isAdmin();

                header('Location: /');
            else:

                header('Location: /aanmelden?err=voor%20emailadres%20' . $user->getEmail() . '%20bestaat%20al%20een%20user.');
            endif;
        else:
            header('Location: /aanmelden?err=Vull%20de%20gegevens%20volledig%20in.');
        endif;
    }
}