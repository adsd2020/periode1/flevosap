<?php


class LoginController
{
    public function index()
    {
        require 'views/header.view.php';
        require 'views/login.view.php';
        require 'views/footer.temp.view.php';
    }

    public function login()
    {
        if (isset($_POST['password']) && strlen($_POST['password']) > 0) {
            $user = UserModel::getByEmail($_POST['email']);

            if ($user !== false):
                if ($user->getPassword() == sha1($_POST['password'])) {
                    $_SESSION['userId'] = $user->getId();
                    $_SESSION['userName'] = $user->getName();
                    $_SESSION['userAdmin'] = $user->isAdmin();
                    header('Location: /');
                } else {
                    header('Location: /inloggen?err=invalid%20password');
                }
            else:
                header('Location: /inloggen?err=Deze%20gebruiker%20is%20nog%20niet%20geregistereed.');
            endif;
        } else {
            header('Location: /inloggen?err=Info%20empty');
        }
    }

    public function logOut()
    {
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        unset($_SESSION['userAdmin']);
        header('Location: /');
    }

}