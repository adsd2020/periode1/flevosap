# SHOW TABLES;

CREATE TABLE user
(
    id         INT AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
    name       VARCHAR(100)       NOT NULL,
    password   VARCHAR(255)       NOT NULL,
    email      VARCHAR(100)       NOT NULL UNIQUE,
    street     VARCHAR(100)       NOT NULL,
    homeNumber INT                NOT NULL,
    zipcode    VARCHAR(50)        NOT NULL,
    state      VARCHAR(100)       NOT NULL,
    admin      BOOLEAN            NOT NULL
);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Admin', SHA1('Admin'), 'admin@flevosap.windesheim.nl', '', 0, '', '', true);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Rudy Borgstede', SHA1('123'), 'r.borgstede@windesheim.nl','Hospitaaldreef',5,'1315 RC','Almere', true);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Stephan Hoeksema', SHA1('123'), 's.hoeksema@windesheim.nl','Hospitaaldreef',5,'1315 RC','Almere', true);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Matthijs de Vos', SHA1('123'), 'mrc.vos@windesheim.nl','Hospitaaldreef',5,'1315 RC','Almere', true);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Fake Zijl', SHA1('123'), 'fe.zijl@windesheim.nl','Hospitaaldreef',5,'1315 RC','Almere', true);

INSERT INTO user (name, password, email, street, homeNumber, zipcode, state, admin) VALUE
    ('Ali Saviz', SHA1('123'), 'wfsdad20b3@gmail.com','Kastanjelaan',25,'9363 CV','Marum', false);



# DROP TABLE user;
# DESCRIBE user;
# SELECT *
# FROM user;
# delete from user WHERE id = 3;
# UPDATE user SET admin = false WHERE id=6;

CREATE TABLE product
(
    id            INT AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
    productNumber MEDIUMINT          NOT NULL UNIQUE,
    name          VARCHAR(100)       NOT NULL,
    picture       LONGBLOB           NOT NULL,
    picMime       VARCHAR(20)        NOT NULL,
    description   LONGTEXT           NOT NULL,
    price         DOUBLE(6, 4)       NOT NULL
);

# DROP TABLE product;
# DESCRIBE product;
# SELECT *
# FROM product;

CREATE TABLE salesOrder
(
    id          INT AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
    orderDate   DATETIME           NOT NULL DEFAULT NOW(),
    orderNumber BIGINT             NOT NULL UNIQUE,
    userId      INT                NOT NULL,
    FOREIGN KEY (userId) REFERENCES user (id)
);

# SELECT *
# FROM salesorder;
# DROP TABLE salesOrder;
# DESCRIBE salesOrder;

CREATE TABLE salesOrderRows
(
    id        INT AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
    orderId   INT                NOT NULL,
    productId INT                NOT NULL,
    quantity  INT                NOT NULL CHECK ( quantity > 0 ),
    price     DOUBLE             NOT NULL,
    FOREIGN KEY (orderId) REFERENCES salesOrder (id),
    FOREIGN KEY (productId) REFERENCES product (id)
);

# SELECT * FROM salesorderrows;
# DESCRIBE salesOrderRows;
# DROP TABLE salesOrderRows;