<?php


class SalesOrderRows extends Model
{
    private $id;
    private $orderId;
    private $productId;
    private $quantity;
    private $price;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    private function setInfo($result)
    {
        $this->id = $result['id'];
        $this->orderId = $result['orderId'];
        $this->productId = $result['productId'];
        $this->quantity = $result['quantity'];
        $this->price = $result['price'];
    }

    public static function getById($id)
    {
        self::load();
        $query = 'SELECT * FROM salesOrderRows WHERE id=:id';
        if ($stmt = parent::$pdo->prepare($query)):
            $orderRow = new self();

            $stmt->bindParam(':id', $id);
            $stmt->execute();

            if ($stmt->rowCount() > 0):
                $result = $stmt->fetch();
                $orderRow->setInfo($result);

                return $orderRow;
            endif;
        endif;
        return false;
    }

    public static function getOrderRows($orderId): array
    {
        $list = [];
        self::load();
        $query = 'SELECT * FROM salesOrderRows WHERE orderId=:orderId;';
        if ($stmt = parent::$pdo->prepare($query)):
            $stmt->bindParam(':orderId', $orderId);
            $stmt->execute();

            if ($stmt->rowCount() > 0):
                $result = $stmt->fetchAll();

                foreach ($result as $row):
                    $row = new self();

                    $row->setInfo($row);
                    array_push($list, $row);
                endforeach;
            endif;
        endif;
        return $list;
    }

    public function saveToDB(): void
    {
        self::load();

        if (isset($this->id) && $this->id > 0):
            $query = 'UPDATE salesOrderRows SET orderId=:orderId, productId=:productId, quantity=:quantity, price=:price;';
        else:
            $query = 'INSERT INTO salesOrderRows (orderId, productId, quantity, price) VALUE (:orderId, :productId, :quantity, :price)';
        endif;

        $stmt = parent::$pdo->prepare($query);
        $stmt->bindParam(':orderId', $this->orderId);
        $stmt->bindParam(':productId', $this->productId);
        $stmt->bindParam(':quantity', $this->quantity);
        $stmt->bindParam(':price', $this->price);

        $stmt->execute();

        if (!isset($this->id)):
            $this->id = parent::$pdo->lastInsertId('salesOrderRows');
        endif;
    }
}