<?php


class ProductModel extends Model
{

    /**Atributes*/
    private int $id, $productNumber;
    private string $name, $description, $picMime;
    private string $picture;
    private float $price;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getProductNumber(): int
    {
        return $this->productNumber;
    }

    /**
     * @param int $productNumber
     */
    public function setProductNumber(int $productNumber): void
    {
        $this->productNumber = $productNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getPicMime(): string
    {
        return $this->picMime;
    }

    /**
     * @param string $picMime
     */
    public function setPicMime(string $picMime): void
    {
        $this->picMime = $picMime;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public static function getById($id)
    {
        $product = new self();
        self::load();
        $stmt = parent::$pdo->prepare('SELECT * FROM product WHERE id=:id;');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $count = $stmt->rowCount();

        if ($count > 0) {
            $result = $stmt->fetch();

            $product->setInfo($result);
        }

        return $product;
    }

    public static function getByProductNumber($productNumber)
    {
        $product = new self();
        self::load();

        $stmt = parent::$pdo->prepare('SELECT * FROM product WHERE productNumber=:productNumber;');
        $stmt->bindParam(':productNumber', $productNumber);
        $stmt->execute();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $result = $stmt->fetch();

            $product->setInfo($result);
        }

        return $product;
    }

    private function setInfo($result)
    {
        $this->id = $result['id'];
        $this->productNumber = $result['productNumber'];
        $this->name = $result['name'];
        $this->picture = $result['picture'];
        $this->picMime = $result['picMime'];
        $this->description = $result['description'];
        $this->price = $result['price'];
    }

    public function saveToDB()
    {
        self::load();
        if (isset($this->id) && $this->id > 0) {
            $stmt = Model::$pdo->prepare('UPDATE product SET productNumber=:productNumber, name=:name, picture=:picture, picMime=:picMime, description=:description, price=:price WHERE id=:id;');
        } else {
            $stmt = Model::$pdo->prepare('INSERT INTO product
                                (productNumber, name, picture, picMime, description, price) VALUE (
                                :productNumber, :name, :picture, :picMime, :description, :price);');
        }

        $stmt->bindParam(':productNumber', $this->productNumber);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':picture', $this->picture, parent::$pdo::PARAM_LOB);
        $stmt->bindParam(':picMime', $this->picMime);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':price', $this->price);

        $stmt->execute();

        if (!isset($this->id)):
            $this->id = parent::$pdo->lastInsertId('product');
        endif;
    }

    public static function getAll()
    {
        $list = [];
        self::load();

        if ($stmt = parent::$pdo->prepare('SELECT * FROM product;')):
            $stmt->execute();
            $result = $stmt->fetchAll();


            foreach ($result as $row):
                $product = new self();

                $product->setId($row['id']);
                $product->setProductNumber($row['productNumber']);
                $product->setName($row['name']);
                $product->setPicture($row['picture']);
                $product->setPicMime($row['picMime']);
                $product->setDescription($row['description']);
                $product->setPrice($row['price']);

                array_push($list, $product);
            endforeach;
        endif;
        return $list;
    }

}