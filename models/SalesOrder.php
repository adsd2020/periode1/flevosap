<?php


class SalesOrder extends Model
{
    private int $id, $userId;
    private float $orderNumber;
    private $orderDate;
    private array $orderRows;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float
     */
    public function getOrderNumber(): float
    {
        return $this->orderNumber;
    }

    /**
     * @param float $orderNumber
     */
    public function setOrderNumber(float $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate->format("d M Y");
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $orderDate = new DateTime($orderDate);

        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getOrderRows()
    {
        return $this->orderRows;
    }

    /**
     * @param mixed $orderRows
     */
    public function setRows($orderRows): void
    {
        $this->orderRows = $orderRows;
    }

    private function setInfo($result)
    {
        $this->id = $result['id'];
        $this->orderDate = $result['orderDate'];
        $this->orderNumber = $result['orderNumber'];
        $this->userId = $result['userId'];
    }

    public static function getById($orderId)
    {
        $query = 'SELECT * FROM salesOrder WHERE id = :id;';
        self::load();
        if ($stmt = parent::$pdo->prepare($query)):
            $saleOrder = new self();

            $stmt->bindParam(":id", $orderId);
            $stmt->execute();
            if ($stmt->rowCount() > 0):
                $result = $stmt->fetch();
                $saleOrder->setInfo($result);

                $saleOrder->orderRows = SalesOrderRows::getOrderRows($orderId);

                return $saleOrder;
            endif;
        endif;

        return false;
    }

    public static function getByOrderNumber($orderNumber)
    {
        $query = 'SELECT * FROM salesOrder WHERE orderNumber = :orderNumber;';
        self::load();
        if ($stmt = parent::$pdo->prepare($query)):
            $saleOrder = new self();

            $stmt->bindParam(":id", $orderId);
            $stmt->execute();
            if ($stmt->rowCount() > 0):
                $result = $stmt->fetch();
                $saleOrder->setInfo($result);

                $saleOrder->orderRows = SalesOrderRows::getOrderRows($saleOrder->id);

                return $saleOrder;
            endif;
        endif;

        return false;
    }

    public function saveToDB()
    {
        self::load();
        if (isset($this->id) && $this->id > 0) {
            $stmt = Model::$pdo->prepare('UPDATE salesOrder SET orderDate=:orderDate, orderNumber=:orderNumber, userId=:userId WHERE id=:id;');
        } else {
            $stmt = Model::$pdo->prepare('INSERT INTO salesOrder
                                (orderDate, orderNumber, userId) VALUE (
                                :orderDate , :orderNumber, :userId);');
        }
        $stmt->bindValue(':orderDate', $this->orderDate->format("Y-m-d h:i:s"));
        $stmt->bindParam(':orderNumber', $this->orderNumber);
        $stmt->bindParam(':userId', $this->userId);

        $stmt->execute();

        if (!isset($this->id)):
            $this->id = parent::$pdo->lastInsertId('salesOrder');
        endif;

        foreach ($this->orderRows as $orderRow):

            $orderRow->setOrderId($this->id);
            $orderRow->saveToDB();
        endforeach;


    }

    public static function getLastOrderNumber()
    {
        self::load();
        $stmt = parent::$pdo->prepare('SELECT MAX(orderNumber) AS MAXNr FROM salesOrder;');
        $stmt->execute();
        return floatval($stmt->fetch()['MAXNr']);
    }

}