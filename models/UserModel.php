<?php

class UserModel extends Model
{

    /* Attributes */
    private int $id, $homeNumber;
    private string $name, $password, $email, $street, $zipcode, $state;
    private bool $admin;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getHomeNumber(): int
    {
        return $this->homeNumber;
    }

    /**
     * @param int $homeNumber
     */
    public function setHomeNumber(int $homeNumber): void
    {
        $this->homeNumber = $homeNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = trim($name, ' ');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = sha1($password);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = trim($email, ' ');
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = trim($street, ' ');
    }

    /**
     * @return string
     */
    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode(string $zipcode): void
    {
        $this->zipcode = trim($zipcode, ' ');
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = trim($state, ' ');
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     */
    public function setAdmin(bool $admin): void
    {
        $this->admin = $admin;
    }

    /* methods */
    public function saveToDB()
    {
        self::load();

        if (isset($this->id) && $this->id > 0) {
            $stmt = Model::$pdo->prepare('UPDATE user SET name=:name, password=:password, email=:email, street=:street, homeNumber=:homeNumber, zipcode=:zipcode, state=:state, admin=:admin WHERE id=:id;');
            $stmt->bindParam(':id', $this->id);
        } else {
            $stmt = Model::$pdo->prepare('INSERT INTO user
                                (name, password, email, street, homeNumber, zipcode, state, admin ) VALUE (
                                :name, :password, :email, :street, :homeNumber, :zipcode, :state,:admin);');

        }
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':street', $this->street);
        $stmt->bindParam(':homeNumber', $this->homeNumber);
        $stmt->bindParam(':zipcode', $this->zipcode);
        $stmt->bindParam(':state', $this->state);
        $stmt->bindParam(':admin', $this->admin, PDO::PARAM_BOOL);

        $stmt->execute();

        if (!isset($this->id)):
            $this->id = parent::$pdo->lastInsertId('user');
        endif;

    }

    public static function removeFromDB($id)
    {
        self::load();

        $stmt = parent::$pdo->prepare('DELETE FROM user WHERE id = :id;');
        $stmt->bindParam(':id',$id);
        $stmt->execute();

    }

    public static function getById($id)
    {
        $user = new self();
        self::load();

        $stmt = parent::$pdo->prepare('SELECT * FROM user WHERE id=:id;');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $count = $stmt->rowCount();

        if ($count > 0) {
            $result = $stmt->fetch();

            $user->setInfo($result);
        }

        return $user;
    }

    public static function getByEmail(string $email)
    {
        $user = new self();
        self::load();

        if ($stmt = parent::$pdo->prepare('SELECT * FROM user WHERE email=:email;')):
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetch();

            if ($result !== false) :
                $user->setInfo($result);
                return $user;
            endif;
        endif;

        return false;
    }

    private function setInfo($result)
    {
        $this->id = $result['id'];
        $this->name = $result['name'];
        $this->password = $result['password'];
        $this->email = $result['email'];
        $this->street = $result['street'];
        $this->homeNumber = $result['homeNumber'];
        $this->zipcode = $result['zipcode'];
        $this->state = $result['state'];
        $this->admin = $result['admin'];
    }
}