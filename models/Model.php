<?php


class Model
{
    protected static PDO $pdo;

    protected static function load()
    {
        if (!isset(Model::$pdo)):
            try {
//                Model::$pdo = new PDO('mysql:host=localhost;dbname=teamb3_flevosap', 'teamb3', 'teamb3', [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
                Model::$pdo = new PDO('mysql:host=localhost;dbname=flevosap', 'root', 'Admin', [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
            } catch (PDOException $e) {
                die('Connection failed: ' . $e->getMessage());
            }
        endif;
    }
}