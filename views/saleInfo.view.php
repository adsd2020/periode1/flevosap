<?php if ($user === false): ?>
    <div class="container aanmeldform">
        <div class="row aanmeldtitle">
            <h1>Inlog formulier</h1>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <br/>
                <div class="alert alert-warning" role="alert">
                    u moet ingelogd zijn om uw bestelling verder kunnen afhandelen.
                </div>

                <form method="post" action="/payOrder">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">E-mail:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="email" id="inputEmail3" name="email" placeholder="E-mail"
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Wachtwoord:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="password" name="password" placeholder="Wachtwoord"
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-primary mb-2 alignright">Inloggen</button>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                </form>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
<?php else: ?>
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                Uw bestelling wordt verstuurd naar de volgende adres. mocht het adres niet kloppen of wilt u uw
                bestelling
                naar een andere adres willen hebben, wijzig eerst uw gegevens en rond daarna uw bestelling af. <br><br>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="row verticalSplit">
            <div class="col-3"></div>
            <div class="col-2">
                <strong>
                    Naam:<br>
                    E-mail:<br>
                    Aders:
                </strong>
            </div>
            <div class="col-4">
                <?php

                echo $user->getName() . "<br />";
                echo $user->getEmail() . "<br />";
                echo $user->getStreet() . " " . $user->getHomeNumber() . "<br />";
                echo $user->getZipcode() . " " . $user->getState();

                ?>
            </div>
            <div class="col-3"></div>
        </div>
        <?php
        $subTotal = 0;
        if (count($products) > 0 && $articles !== false):
            ?>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col" class="textAlignCentre">Aantal</th>
                            <th scope="col" class="textAlignCentre">€ Prijs</th>
                            <th scope="col" class="textAlignCentre">€ Subtotaal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach (array_keys($articles) as $productId):
                            $product = $products[$productId];
                            $subTotal += $articles[$productId]['quantity'] * $product->getPrice(); ?>
                            <tr>
                                <td><?php echo $product->getName(); ?></td>
                                <td class="textAlignRight paddingSet"><?php echo number_format($articles[$productId]['quantity'], 2, ',', '.') ?></td>
                                <td class="textAlignRight paddingSet"><?php echo number_format($product->getPrice(), 2, ',', '.') ?></td>
                                <td class="textAlignRight paddingSet"><?php echo number_format($articles[$productId]['quantity'] * $product->getPrice(), 2, ',', '.') ?></td>
                            </tr>
                        <?php
                        endforeach; ?>
                        <tr>
                            <td colspan="3" class="textAlignRight paddingSet"><strong>Totaal:</strong></td>
                            <td class="textAlignRight paddingSet">
                                <strong>€ <?php echo number_format($subTotal, 2, ',', '.') ?></strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-2"></div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <form action="finishShopping" method="post">
                        <button type="submit" class="btn btn-primary btnAddToCart">Betalen</button>
                    </form>
                </div>
                <div class="col-2"></div>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
