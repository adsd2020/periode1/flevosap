<br/>
<div class="container">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <?php if (isset($_GET['err'])): ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_GET['err'] ?>
                </div>
            <?php endif; ?>

            <form method="post" action="/importProduct" enctype="multipart/form-data">
                <div class="form-group">
                    Product nummer: <br/>
                    <input name="productNumber" class="form-control" type="text" placeholder="Product nummer"><br/>
                    naam: <br/>
                    <input name="name" type="text" class="form-control" placeholder="name"><br/>
                    <input type="file" class="form-control-file" name="productPicture" accept="image/*"><br/>
                    Omschrijving: <br/>
                    <textarea class="form-control" name="description"></textarea><br/>
                    Price: <br/>
                    <input type="number" class="form-control" name="price" step="any"><br/>
                    <button type="submit" class="btn btn-primary mb-2 alignright">importeer</button>
                </div>
            </form>
        </div>
        <div class="col-3"></div>
    </div>
</div>

