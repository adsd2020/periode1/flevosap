<div class="container">
    <div class="row">

        <?php foreach ($products as $product): ?>
            <div class="col-3">
                <div class="card shadow-sm">
                    <div class="cardimgtop">
                        <img class="card-img-top" src="/productImage?id=<?php echo $product->getId() ; ?>"
                             alt="<?php echo $product->getName(); ?>">
                    </div>
                    <div class="card-body">
                        <div class="productData">
                            <h4 class="card-title"><?php echo $product->getName() ?></h4>
                            <div class="card-text"><?php echo $product->getDescription() ?></div>
                        </div>
                        <div class="productPrijs">€ <?php echo number_format($product->getPrice(), 2, ",", "."); ?></div>
                        <form method="post" action="/addToShoppingCart">
                            <input type="number" class="form-control quantityInput"
                                   name="quantity" placeholder="aantal" min="1"
                                   oninput="validity.valid||(value = '');" required>
                            <input name="productId" hidden
                                   value="<?php echo $product->getId(); ?>">
                            <button class="btn btn-primary btnAddToCart" type="submit">
                                <img src="../asset/Pictures/baseline_add_shopping_cart_black_18dp.png" alt="toevoegen"
                                     width="28px" height="28px">
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>