<div class="container passwordform">
    <div class="row passwordtitle">
        <h1>Wachtwoord veranderen</h1>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">

            <?php if (isset($_GET['err'])): ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_GET['err'] ?>
                </div>
            <?php endif; ?>

            <form method="post" action="/passChange" onsubmit="return validationPassRep()">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Wachtwoord:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="password" placeholder="Wachtwoord" required
                               onblur="pasMatch()">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Wachtwoord herhalen:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="passwordRep" placeholder="Wachtwoord herhalen"
                               required onblur="pasMatch()">
                    </div>
                    <div class="col-sm-1">
                        <span class="badge badge-danger" id="passBadgSpan" hidden>ongelijk <br/> wachwoord</span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary mb-2 alignright">Wijzigen</button>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </form>
        </div>
        <div class="col-1"></div>
    </div>
</div>