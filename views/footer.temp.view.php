<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-4 logo">
                <img src="../asset/Pictures/logo-headerV3.png" alt=" Flevosap"">
            </div>
            <div class="col-4 gegevensbloc">
                Flevosap bv <br/>
                Prof. Zuurlaan 22<br/>
                8256 PE Biddinghuizen, Nederland<br/>
                Tel: +31 (0)321 – 33 25 25<br/>
                <a href="mailto:info@flevosap.nl">info@flevosap.nl</a>
            </div>
            <div class="col-4 SocialMedia">
                Volg ons op <br/>
                <img src="../asset/Pictures/facebook-1.jpg" alt="">
            </div>
        </div>
    </div>
</div>

<!-- tijdelijk voor test -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!-- tijdelijk voor test -->

<script type="text/javascript" src="views/Scripts/myScripts.js"></script>