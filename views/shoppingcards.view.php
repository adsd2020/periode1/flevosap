<div class="container shoppingcartscontainer">
    <?php if (isset($_GET['err'])): ?>
        <br/>
        <div class="alert alert-danger" role="alert">
            <?php echo $_GET['err'] ?>
        </div>
    <?php endif; ?>

    <table class="table table-striped">
        <thead>
        <tr>
            <td colspan="6" class="tableheader"><h1>Winkelwagen</h1></td>
        </tr>
        <tr>
            <th scope="col"></th>
            <th scope="col">Product</th>
            <th scope="col" class="textAlignCentre">Hoeveelheid</th>
            <th scope="col" class="textAlignCentre">€ Prijs</th>
            <th scope="col" class="textAlignCentre">€ Subtotaal</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <?php $subTotal = 0;
        if (count($products)>0 && $articles !== false): ?>
            <tbody>
            <?php
            foreach (array_keys($articles) as $articleId):
                $product = $products[$articleId];
                $subTotal += $articles[$articleId]['quantity'] * $product->getPrice(); ?>
                <tr>
                    <th scope="row">
                        <div class="fotoshoppingcart">
                            <img class="card-img-top"
                                 src="/productImage?id=<?php echo $articleId; ?> alt="<?php echo $articleId ?>>
                        </div>
                    </th>
                    <td><?php echo $product->getName(); ?></td>
                    <td class="textAlignRight paddingSet"><?php echo number_format($articles[$articleId]['quantity'], 0, ',', '.'); ?></td>
                    <td class="textAlignRight paddingSet"><?php echo number_format($product->getPrice(), 2, ',', '.'); ?></td>
                    <td class="textAlignRight paddingSet"><?php echo number_format($product->getPrice() * $articles[$articleId]['quantity'], 2, ',', '.'); ?></td>
                    <td class="buttonsCell">
                        <div class="row">
                            <div class="form-group">
                                <form method="post" action="/removeFromShoppingCart" class="shoppingcartbuttons">
                                    <input type="hidden" name="productId" value="<?php echo $articleId ?>">
                                    <button type="submit" name="increase" class="btn btn-success mb-2"
                                            formaction="/increaseShoppingCart">+
                                    </button>
                                    <button type="submit" name="decrease" class="btn btn-warning mb-2"
                                            formaction="/decreaseShoppingCart">-
                                    </button>
                                    <button type="submit" name="remove" class="btn btn-danger mb-2">x</button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4" class="textAlignRight paddingSet boldTxt">Subtotaal:</td>
                <td class="textAlignRight paddingSet boldTxt">€ <?php echo number_format($subTotal,2,',','.'); ?></td>
                <td class="tableFooter">
                    <form method="post" action="/payOrder" class="alignright">
                        <input type="submit" class="btn btn-primary" value="Voltooien">
                    </form>
                </td>
            </tr>
            </tbody>
        <?php endif; ?>
    </table>
</div>