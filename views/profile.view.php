<div class="container profileform">
    <div class="row profiletitle">
        <h1>Uw profiel</h1>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">

            <?php if (isset($_GET['err'])): ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_GET['err'] ?>
                </div>
            <?php endif; ?>

            <form method="post" action="/profile">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="name" placeholder="Naam" value="<?php echo $user->getName()?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">E-mail:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="email" id="inputEmail3" name="email" placeholder="E-mail" value="<?php echo $user->getEmail() ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Adres en huisnr.:</label>
                    <div class="col-sm-6">
                        <input class="form-control" type="text" name="street" placeholder="Straat" value="<?php echo $user->getStreet()?>" required>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control" type="number" name="homeNumber" placeholder="nr." value="<?php echo $user->getHomeNumber()?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Postcode en woon plaats:</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="zipcode" placeholder="Postcode" value="<?php echo $user->getZipCode()?>" required>
                    </div>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="state" placeholder="Woonplaats" value="<?php echo $user->getState()?> " required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary mb-2 alignright">Wijzigen</button>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <div class="form-group row">
                    <span class="formLink"><a href="/passChange">Klik hier om je wachtwoord te wijzigen</a></span>
                </div>
            </form>
        </div>
        <div class="col-1"></div>
    </div>
</div>