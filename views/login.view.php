<div class="container aanmeldform">
    <div class="row aanmeldtitle">
        <h1>Inlog formulier</h1>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">

            <?php if (isset($_GET['err'])): ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_GET['err'] ?>
                </div>
            <?php endif; ?>

            <form method="post" action="/inloggen">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">E-mail:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="email" id="inputEmail3" name="email" placeholder="E-mail"
                               required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Wachtwoord:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="password" placeholder="Wachtwoord" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary mb-2 alignright">Inloggen</button>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <div class="form-group row">
                    <span class="formLink"><a href="/aanmelden">Nieuwe account aanmaken</a></span>
                </div>

            </form>
        </div>
        <div class="col-1"></div>
    </div>
</div>