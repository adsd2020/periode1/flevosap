<div class="container aanmeldform">
    <div class="row aanmeldtitle">
        <h1>Aanmeld formulier</h1>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">

            <?php if (isset($_GET['err'])): ?>
                <br/>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_GET['err'] ?>
                </div>
            <?php endif; ?>

            <form method="post" action="/aanmelden" onsubmit="return validationPassRep()">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Name:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="name" placeholder="Naam" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Wachtwoord:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="password" placeholder="Wachtwoord" required
                               onblur="pasMatch()">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Wachtwoord herhalen:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="passwordRep" placeholder="Wachtwoord herhalen"
                               required onblur="pasMatch()">
                    </div>
                    <div class="col-sm-1">
                        <span class="badge badge-danger" id="passBadgSpan" hidden>ongelijk <br/> wachwoord</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">E-mail:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="email" id="inputEmail3" name="email" placeholder="E-mail"
                               required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Adres en huisnr.:</label>
                    <div class="col-sm-6">
                        <input class="form-control" type="text" name="street" placeholder="Straat" required>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control" type="number" name="homeNumber" placeholder="nr." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Postcode en woon plaats:</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="zipcode" placeholder="Postcode" required>
                    </div>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="state" placeholder="Woonplaats" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary mb-2 alignright">Aanmelden</button>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </form>
        </div>
        <div class="col-1"></div>
    </div>
</div>