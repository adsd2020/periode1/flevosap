<?php
$uri = trim($_SERVER['REQUEST_URI'], '/');
$uri = explode('?',$uri)[0];
?>

<!doctype html>
<html lang="en">
<head>
    <title>Flevosap team B3</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/views/Css/myCss.css" type="text/css">

    <link rel="stylesheet" id="avia-google-webfont" href="//fonts.googleapis.com/css?family=Open+Sans:400,600"
          type="text/css" media="all">
</head>
<body>
<header class="header">
    <div class="container">
        <div class="row">
            <span class="logo col-2">
                <img src="../asset/Pictures/logo-headerV3.png" alt="Flevosap">
            </span>
            <nav class="nav">
                <a class="<?php echo ($uri === "home") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/">Thuis</a>
                <a class="<?php echo ($uri === "Boomgaard") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/boomgaard/">Boomgaard</a>
                <a class="<?php echo ($uri === "voorzichtig") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/voorzichtig/">In de fles</a>
                <a class="<?php echo ($uri === "hmmm") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/hmmm/">hmmm</a>
                <div class="dropdown">
                    <a class="<?php echo ($uri === "voedingsinfo" || $uri === "groentesap") ? "nav-link active" : "nav-link" ?>"
                       href="#">
                        Sappen
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="https://www.flevosap.nl/voedingsinfo/">Vruchtensap</a>
                        <a class="dropdown-item" href="https://www.flevosap.nl/groentesap/">Groentesap</a>
                    </div>
                </div>
                <a class="<?php echo ($uri === "Horeca") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/horeca/">Horeca</a>
                <a class="<?php echo ($uri === "nieuws") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/nieuws/">Nieuws</a>
                <a class="<?php echo ($uri === "contact") ? "nav-link active" : "nav-link" ?>"
                   href="https://www.flevosap.nl/contact/">Contact</a>
                <a class="<?php echo ($uri === "") ? "nav-link active" : "nav-link" ?>" href="/">Webshop</a>
                <div class="nav-link navSeparator"></div>
                <a class="nav-link" href="winkelwagen">
                    <img src="../asset/Pictures/baseline_shopping_cart_black_18dp.png" alt="Winkelwagen">
                    <span class="badge badge-pill badge-danger" <?php echo (ShoppingCartController::getShoppingCart() === false || count(ShoppingCartController::getShoppingCart())===0) ? "hidden" : "" ?> >
                        <?php echo (ShoppingCartController::getShoppingCart() === false) ? "" : count(ShoppingCartController::getShoppingCart()) ?>
                    </span>
                </a>

                <?php if (isset($_SESSION['userId'])): ?>
                <div class="dropdown">
                    <?php endif; ?>

                    <a class="<?php echo ($uri === "aanmelden" || $uri === "inlogen") ? "nav-link active" : "nav-link" ?>"
                       href="<?php echo (isset($_SESSION['userId'])) ? "#" : "/inloggen"; ?>">
                        <?php if (isset($_SESSION['userName'])):
                            $firstWord = explode(" ", $_SESSION['userName']);
                            echo "Welkom " . $firstWord[0];
                        else:
                            echo "Inloggen";
                        endif;
                        ?>
                    </a>

                    <?php if (isset($_SESSION['userAdmin'])): ?>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/profile">Profile</a>
                            <?php if ($_SESSION['userAdmin']): ?>
                                <a class="dropdown-item" href="importproduct">import product</a>
                            <?php endif; ?>
                            <a class="dropdown-item" href="/logOut">Loguit</a>
                        </div>
                    <?php endif; ?>
                </div">

            </nav>
        </div>
    </div>
</header>
<div class="row topBanner">
    <img src="../asset/Pictures/Assortiment-Flevosap-achtergrondB.jpg" alt="" class="headerBanner"/>
</div>
