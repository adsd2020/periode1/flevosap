<?php

$router->define([
    'GET' => [
        '' => [
            'controller' => 'HomeController',
            'methode' => 'index',
            'authentication' => false,
            'adminOnly' => false],

        'aanmelden' => [
            'controller' => 'RegisterController',
            'methode' => 'index',
            'authentication' => false,
            'adminOnly' => false],

        'inloggen' => [
            'controller' => 'LoginController',
            'methode' => 'index',
            'authentication' => false,
            'adminOnly' => false],

        'logOut' => [
            'controller' => 'LoginController',
            'methode' => 'logOut',
            'authentication' => false,
            'adminOnly' => false],

        'importproduct' => [
            'controller' => 'ProductController',
            'methode' => 'index',
            'authentication' => true,
            'adminOnly' => true],

        'productImage' => [
            'controller' => 'ProductController',
            'methode' => 'getProductImage',
            'authentication' => false,
            'adminOnly' => false],

        'winkelwagen' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'index',
            'authentication' => false,
            'adminOnly' => false],

        'profile' => [
            'controller' => 'ProfileController',
            'methode' => 'index',
            'authentication' => true,
            'adminOnly' => false],


        'passChange' => [
            'controller' => 'ProfileController',
            'methode' => 'index2',
            'authentication' => true,
            'adminOnly' => false],

        'nietgevonden' => [
            'controller' => 'HomeController',
            'methode' => 'notFound',
            'authentication' => false,
            'adminOnly' => false],


        /**
         * temporary route to full database
         */
        'importFromCSV' => ['controller' => 'ProductController', 'methode' => 'importFromCSV', 'authentication' => true, 'adminOnly' => true]

    ],
    'POST' => [
        'inloggen' => [
            'controller' => 'LoginController',
            'methode' => 'login',
            'authentication' => false,
            'adminOnly' => false],

        'aanmelden' => [
            'controller' => 'RegisterController',
            'methode' => 'register',
            'authentication' => false,
            'adminOnly' => false],

        'importProduct' => [
            'controller' => 'ProductController',
            'methode' => 'importProduct',
            'authentication' => true,
            'adminOnly' => true],

        'addToShoppingCart' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'addProductToShoppingCart',
            'authentication' => false,
            'adminOnly' => false],

        'removeFromShoppingCart' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'removeProductFromShoppingCart',
            'authentication' => false,
            'adminOnly' => false],

        'increaseShoppingCart' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'increaseProduct',
            'authentication' => false,
            'adminOnly' => false],

        'decreaseShoppingCart' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'decreaseProduct',
            'authentication' => false,
            'adminOnly' => false],

        'finishShopping' => [
            'controller' => 'ShoppingCartController',
            'methode' => 'finishShopping',
            'authentication' => false,
            'adminOnly' => false],

        'payOrder'=> [
            'controller' => 'ShoppingCartController',
            'methode' => 'saleInfo',
            'authentication' => false,
            'adminOnly' => false],

        'profile' => [
            'controller' => 'ProfileController',
            'methode' => 'updateInfo',
            'authentication' => true],

        'passChange' => [
            'controller' => 'ProfileController',
            'methode' => 'updatePassword',
            'authentication' => true,
            'adminOnly' => false]
    ]
]);