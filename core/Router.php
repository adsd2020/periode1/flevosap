<?php


class Router
{
    protected $routes = [];

    public function define($routes)
    {
        $this->routes = $routes;
    }

    public function direct($uri)
    {
        $reqMeth = $_SERVER['REQUEST_METHOD'];

        if (array_key_exists($uri, $this->routes[$reqMeth])) {
            $routeInfo = $this->routes[$reqMeth][$uri];

            if (($routeInfo['authentication'] === true && isset($_SESSION['userId'])) || ($routeInfo['authentication'] === false)):
                $controller = new $routeInfo['controller']();
                $controller->{$routeInfo['methode']}();
                return;
            else:
                header('Location: /inloggen?err=uw%20moet%20eerst%20inloggen.');
            endif;
        } else {
            header('Location /nietgevonden');
        }

        throw new Exception('Can\'t find the webpage ' . $uri . ' !!', '404');
    }

}